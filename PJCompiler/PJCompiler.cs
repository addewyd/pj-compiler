﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using System.Runtime;

namespace PJCompiler
{

    static class LC
    {
        public static System.IO.TextWriter sv = null;
        public static string FileName;
        public static string ScriptName;

        public static void Open(String f)
        {
            FileName = f;
            if(System.IO.File.Exists(FileName))
            {
                System.IO.File.Delete(FileName);
            }
            sv = System.IO.File.AppendText(FileName);
        }

        public static void Close()
        {
            sv.Close();
        }

    }

    // .....................................................................................

    class Visitor : pjc2BaseVisitor<string>
    {

        Stack<string> stack;
        Stack<int> parents;

        System.IO.StringWriter svg;
        int optionId;
        int parent;
        int id;
        string atext;
        string scriptName;
        bool isPlist;

        Dictionary<string, string> xmlnsFromType;

        // .................................................................................................

        public Visitor()
        {
            stack = new Stack<string>();
            parents = new Stack<int>();
            svg = new System.IO.StringWriter();
            optionId = -1;
            parent = 0;
            id = 0;
            atext = "";
            isPlist = false;
            xmlnsFromType = new Dictionary<string, string>
            {
                ["HTTPResponses.TypeMethod"] = @"d7p1=""http://schemas.datacontract.org/2004/07/Activities.HTTP""",
                ["int"] = @"d7p1 =""http://www.w3.org/2001/XMLSchema""",
                ["string"] = @"d7p1 =""http://www.w3.org/2001/XMLSchema""",
                ["boolean"] = @"d7p1 =""http://www.w3.org/2001/XMLSchema""",
                ["double"] = @"d7p1 =""http://www.w3.org/2001/XMLSchema""",
                ["ArrayOfArgumentItem"] = @"d7p1=""http://schemas.datacontract.org/2004/07/BR.Core""",
                ["LogLevel"] = @"d7p1=""http://schemas.datacontract.org/2004/07/BR.Core/Base""",
                ["DataTableSortOrder"] = @"d7p1=""http://schemas.datacontract.org/2004/07/Activities.BaseCollections.DataTable"""
            };
        }

        // emits 
        // ...............................................................................................................................

        void StartPropertyValue(int d, int i)
        {
            LC_WriteLine(d, $@"<PropertyValue z:Id=""i{i}"">");
        }

        void EndPropertyValue(int d)
        {
            LC_WriteLine(d, "</PropertyValue>");
        }

        void StartPropertyValue(int d, System.IO.TextWriter w, int i)
        {
            sv_WriteLine(d, w, $@"<PropertyValue z:Id=""i{i}"">");
        }

        void EndPropertyValue(int d, System.IO.TextWriter w)
        {
            sv_WriteLine(d, w, "</PropertyValue>");
        }

        void EmitPropertyName(int d, string v)
        {
            LC_WriteLine(d, $"<PropertyName>{v}</PropertyName>");
        }

        void EmitPropertyName(int d, System.IO.TextWriter w, string v)
        {
            sv_WriteLine(d, w, $"<PropertyName>{v}</PropertyName>");
        }

        void StartAbstractStep(int d, int i)
        {
            LC_WriteLine(d, $@"<AbstractStep z:Id=""i{i}"" i:type=""ExecutedStep"">");
        }

        void EndAbstractStep(int d)
        {
            LC_WriteLine(d, "</AbstractStep>");
        }

        void EmitNewGuid(int d) {
            LC_WriteLine(d, $@"<Id>{Guid.NewGuid()}</Id>");
        }

        void EmitD7p1(int d) {
            LC_WriteLine(d, $@"<_xpathSettings xmlns:d7p1=""http://schemas.datacontract.org/2004/07/BR.XPath"" i:nil=""true"" />");
        }

        void EmitD7p1(int d, System.IO.TextWriter w)
        {
            sv_WriteLine(d, w, $@"<_xpathSettings xmlns:d7p1=""http://schemas.datacontract.org/2004/07/BR.XPath"" i:nil=""true"" />");
        }

        void EmitD9p1(int d, System.IO.TextWriter w)
        {
            sv_WriteLine(d, w, @"<_xpathSettings xmlns:d9p1=""http://schemas.datacontract.org/2004/07/BR.XPath"" i:nil=""true"" />");
        }

        // ...............................................................................................................................

        string correctexpr(string e)
        {
            e = Regex.Replace(e, @"(\[\[)|(\]\])", "");
            e = Regex.Replace(e, @"(\[`)|(`\])", "");
            e = Regex.Replace(e, @"^`", "");
            e = Regex.Replace(e, @"`$", "");
            e = Regex.Replace(e, @"&", "&amp;");
            e = Regex.Replace(e, @"<", "&lt;");
            e = Regex.Replace(e, @">", "&gt;");

            return e;
        }

        public void LC_WriteLine(int depth, String s)
        {
            int n = depth < 2 ? 0 : depth - 2;
            LC.sv.WriteLine(s.PadLeft(s.Length + n * 2));
        }

        // .....................................................................................................
        // to global sv
        void svg_WriteLine(int depth, string s)
        {
            int n = depth < 2 ? 0 : depth - 2;
            svg.WriteLine(s.PadLeft(s.Length + n * 2));
        }

        // .....................................................................................................

        // to local sv
        void sv_WriteLine(int depth , System.IO.TextWriter svl, string s)
        {
            int n = depth < 2 ? 0 : depth - 2;
            svl.WriteLine(s.PadLeft(s.Length + n * 2));
        }

        // .....................................................................................................

        public override string VisitAssignIdent(pjc2Parser.AssignIdentContext ctx)
        {
            string t = ctx.GetText();
            stack.Push(t);
            return t;
        }

        // .....................................................................................................

        public override string VisitAssignExpr(pjc2Parser.AssignExprContext ctx)
        {
            string ident = stack.Pop();
            string expr = correctexpr(ctx.GetText());
            int p = parent;
            ++id;

            StartAbstractStep(ctx.Depth(), id);
            LC_WriteLine(ctx.Depth(), "<Childs />");
            //LC_WriteLine(ctx.Depth() + 1, $@"<Id>{Guid.NewGuid()}</Id>");
            EmitNewGuid(ctx.Depth());

            p = id;
            ++id;

            LC_WriteLine(ctx.Depth() + 1, "<PropertyValues>");
            StartPropertyValue(ctx.Depth() + 2, id);
            LC_WriteLine(ctx.Depth() + 3, "<PropertyName>To</PropertyName>");
            LC_WriteLine(ctx.Depth() + 3, $"<_expression>{ident}</_expression>");
            LC_WriteLine(ctx.Depth() + 3, "<_dataType>Expression</_dataType>");
            EmitD7p1(ctx.Depth() + 3);
            EndPropertyValue(ctx.Depth() + 2);
            ++id;

            StartPropertyValue(ctx.Depth() + 2, id);

            LC_WriteLine(ctx.Depth() + 3, "<PropertyName>Value</PropertyName>");
            LC_WriteLine(ctx.Depth() + 3, $"<_expression>{expr}</_expression>");
            LC_WriteLine(ctx.Depth() + 3, "<_dataType>Expression</_dataType>");
            EmitD7p1(ctx.Depth() + 3);
            EndPropertyValue(ctx.Depth() + 2);

            LC_WriteLine(ctx.Depth() + 1, "</PropertyValues>");
            LC_WriteLine(ctx.Depth() + 1, @"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth() + 1, "<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth() + 1, "<Text></Text>");
            LC_WriteLine(ctx.Depth() + 1, "<_activityFullName>BR.Core.Base.Assign</_activityFullName>");
            EndAbstractStep(ctx.Depth());

            return ident;
        }

        // .....................................................................................................

        public override string VisitScript(pjc2Parser.ScriptContext ctx)
        {
            LC_WriteLine(ctx.Depth(), @"<?xml version=""1.0"" encoding=""utf-8""?>");
            
            //LC.ConsoleWriteLine("Sript: " + ctx.GetText());
            LC_WriteLine(ctx.Depth(), $@"<Script xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"" z:Id=""i{++id}"" xmlns:z=""http://schemas.microsoft.com/2003/10/Serialization/"" xmlns=""http://schemas.datacontract.org/2004/07/BR.Logic"">");
            LC_WriteLine(ctx.Depth(), "<CompilationType>CSharp</CompilationType>");
            LC_WriteLine(ctx.Depth(), $"<Id>{Guid.NewGuid()}</Id>");
            parent = id;
            string s = VisitChildren(ctx);
            LC_WriteLine(ctx.Depth(), svg.ToString());
            LC_WriteLine(ctx.Depth(), @"  <Version i:nil=""true"" />");
            LC_WriteLine(ctx.Depth(), "</Script>");
            LC_WriteLine(ctx.Depth(), "<!-- end -->");
            return s;
        }

        // ..................................................................................................

        // script name and parameters placed at the end

        public override string VisitScriptName(pjc2Parser.ScriptNameContext ctx)
        {
            Console.WriteLine("Sript: " + ctx.GetText());
            scriptName = ctx.GetText() + ".pix";
            LC.ScriptName = scriptName;
            svg_WriteLine(ctx.Depth(), $@"<Name>{scriptName}</Name>");
            return "";
        }

        // .....................................................................................................

        public override string VisitScriptArgList(pjc2Parser.ScriptArgListContext ctx)
        {
            svg_WriteLine(ctx.Depth(), "<Params>");
            string s = VisitChildren(ctx);
            svg_WriteLine(ctx.Depth(), "</Params>");
            return s;
        }

        // .....................................................................................................

        public override string VisitScriptArg(pjc2Parser.ScriptArgContext ctx)
        {
            svg_WriteLine(ctx.Depth(), $@"<Parameter z:Id=""i{++id}"">");
            //parent = id;
            string s = VisitChildren(ctx);
            string argtype = stack.Pop();
            string argname = stack.Pop();
            svg_WriteLine(ctx.Depth(), argname);
            svg_WriteLine(ctx.Depth(), argtype);
            svg_WriteLine(ctx.Depth(), "</Parameter>");
            return s;
        }

        // .....................................................................................................

        public override string VisitDefVal(pjc2Parser.DefValContext ctx)
        {
            var e = correctexpr(ctx.GetText());
            svg_WriteLine(ctx.Depth(), $@"<DefaultValueExpression>{e}</DefaultValueExpression>");
            return "";
        }

        // .....................................................................................................

        public override string VisitScripArgType(pjc2Parser.ScripArgTypeContext ctx)
        {
            stack.Push($@"<TypeString>{Regex.Replace(ctx.GetText(), @"^%", "")}</TypeString>");
            return "";
        }

        // .....................................................................................................

        public override string VisitScriptArgName(pjc2Parser.ScriptArgNameContext ctx)
        {
            stack.Push($@"<Name>{ctx.GetText()}</Name>");
            return "";
        }

        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

        public override string VisitLogic([NotNull] pjc2Parser.LogicContext ctx)
        {
            LC_WriteLine(ctx.Depth(), $@"<Logic z:Id=""i{++id}"" i:type=""ExecutedStep"">");
            parent = id;
            string s = VisitChildren(ctx);
            LC_WriteLine(ctx.Depth() + 1, $@"<Id>{Guid.NewGuid()}</Id>");
            LC_WriteLine(ctx.Depth() + 1, @"<Parent i:nil=""true"" />");
            LC_WriteLine(ctx.Depth() + 1, @"<PropertyValues />");
            LC_WriteLine(ctx.Depth() + 1, @"<SelectedOptionId i:nil=""true"" />");
            LC_WriteLine(ctx.Depth() + 1, @"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth() + 1, @"<Text></Text>");
            LC_WriteLine(ctx.Depth() + 1, "<_activityFullName>BR.Core.Base.Container</_activityFullName>");
            LC_WriteLine(ctx.Depth(), "</Logic>");
            return s;
        }

        // .....................................................................................

        public override string VisitBlock(pjc2Parser.BlockContext ctx)
        {
            LC_WriteLine(ctx.Depth(), "<Childs>");
            string s = VisitChildren(ctx);
            LC_WriteLine(ctx.Depth(), "</Childs>");
            return s;
        }

        // .....................................................................................

        public override string VisitEmptyblock(pjc2Parser.EmptyblockContext ctx)
        {
            LC_WriteLine(ctx.Depth(), "<Childs />");
            return "";
        }

        // ..............................................................................................

        public override string VisitActivity(pjc2Parser.ActivityContext ctx)
        {
            optionId = -1;
            atext = "";
            isPlist = false;
            //
            // Console.WriteLine("ACT: " + ctx.GetText());
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);

            //LC_WriteLine(ctx.Depth() + 1, $"<!-- {ctx.GetText()} -->");

            LC_WriteLine(ctx.Depth() + 1, "<Childs />");
            LC_WriteLine(ctx.Depth() + 1, $"<Id>{Guid.NewGuid()}</Id>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Parent z:Ref=""i{parent}""/>");

            parent = id;

            string s = VisitChildren(ctx);

            if(!isPlist)
            {
                LC_WriteLine(ctx.Depth(), "<PropertyValues />");
            }

            if (optionId >= 0)
            {
                LC_WriteLine(ctx.Depth() + 1, $@"<SelectedOptionId>{optionId}</SelectedOptionId>");
            }
            else
            {
                LC_WriteLine(ctx.Depth() + 1, $@"<SelectedOptionId i:nil=""true""/>");
            }
            LC_WriteLine(ctx.Depth() + 1, $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Text>{atext}</Text>");

            string afn = stack.Pop();
            LC_WriteLine(ctx.Depth(), afn);
            EndAbstractStep(ctx.Depth());
            parent = parents.Pop();
            atext = "";

            return s;
        }

        // .....................................................................................

        public override string VisitAtext(pjc2Parser.AtextContext ctx)
        {
            atext = Regex.Replace(ctx.GetText(), @"(^\"")|(\""$)", "");
            return "";
        }

        // .....................................................................................................

        public override string VisitOption(pjc2Parser.OptionContext ctx)
        {
            optionId = Int32.Parse(ctx.GetText());
            return "";
        }

        // .............................................................................................

        public override string VisitActName(pjc2Parser.ActNameContext ctx)
        {
            string an = Regex.Replace(ctx.GetText(), "^@", "BR.Core.Base.");
            an = Regex.Replace(an, @"^\$", "Activities.");

            // place it at the end
            stack.Push($@"<_activityFullName>{an}</_activityFullName>");
            return "";
        }

        // .............................................................................................

        public override string VisitPlist(pjc2Parser.PlistContext ctx)
        {
            LC_WriteLine(ctx.Depth(), "<PropertyValues>");
            string s = VisitChildren(ctx);
            LC_WriteLine(ctx.Depth(), "</PropertyValues>");
            isPlist = true;
            return s;
        }

        // .............................................................................................

        public override string VisitParam(pjc2Parser.ParamContext ctx)
        {            
            LC_WriteLine(ctx.Depth(), $@"<PropertyValue z:Id=""i{++id}"">");
            string s = VisitChildren(ctx);
            LC_WriteLine(ctx.Depth(), "</PropertyValue>");
            return s;
        }

        // .....................................................................................................

        public override string VisitExprParam(pjc2Parser.ExprParamContext ctx)
        {
            string s = VisitChildren(ctx);
            LC_WriteLine(ctx.Depth(), "<_dataType>Expression</_dataType>");
            EmitD7p1(ctx.Depth());
            return s;
        }

        // .....................................................................................................

        public override string VisitValueParam(pjc2Parser.ValueParamContext ctx)
        {

            string s = VisitChildren(ctx);
            string type = stack.Pop();
            string val = stack.Pop();
            var ns = xmlnsFromType[type];
            string valstr = $@"<_value xmlns:{ns} i:type=""d7p1:{type}"">{val}</_value>";
            LC_WriteLine(ctx.Depth(), $"<_dataType>Value</_dataType>");
            LC_WriteLine(ctx.Depth(), valstr);
            EmitD7p1(ctx.Depth());

            return s;
        }

        // .............................................................................................

        public override string VisitParamType(pjc2Parser.ParamTypeContext ctx)
        {
            string s = $@"{Regex.Replace(ctx.GetText(), "^%", "")}";
            stack.Push(s);
            return s;
        }

        // .............................................................................................

        public override string VisitParamname(pjc2Parser.ParamnameContext ctx)
        {
            EmitPropertyName(ctx.Depth(), ctx.GetText());
            return "";
        }

        // .............................................................................................

        public override string VisitArglist(pjc2Parser.ArglistContext ctx)
        {
            LC_WriteLine(ctx.Depth(), "<PropertyName>Parameters</PropertyName>");
            LC_WriteLine(ctx.Depth(), "<_dataType>Value</_dataType>");
            EmitD7p1(ctx.Depth());
            string s = VisitChildren(ctx);
            LC_WriteLine(ctx.Depth(), "</_value>");
            EmitD7p1(ctx.Depth());
            return s;
        }

        // .....................................................................................................

        public override string VisitArg(pjc2Parser.ArgContext ctx)
        {
            LC_WriteLine(ctx.Depth(), $@"<d7p1:ArgumentItem z:Id=""i{++id}"">");
            string s = VisitChildren(ctx);
            LC_WriteLine(ctx.Depth(), "</d7p1:ArgumentItem>");
            return s;
        }

        // .............................................................................................

        public override string VisitArgName(pjc2Parser.ArgNameContext ctx)
        {
            LC_WriteLine(ctx.Depth(), $@"<d7p1:ArgumentName>{ctx.GetText()}</d7p1:ArgumentName>");
            return "";
        }

        // .............................................................................................

        public override string VisitArgType(pjc2Parser.ArgTypeContext ctx)
        {
            LC_WriteLine(ctx.Depth(), $@"<d7p1:Type>{Regex.Replace(ctx.GetText(), "^%", "")}</d7p1:Type>");
            return "";
        }

        // .....................................................................................................

        public override string VisitArgVal(pjc2Parser.ArgValContext ctx)
        {
            var e = correctexpr(ctx.GetText());

            LC_WriteLine(ctx.Depth(), $@"<d7p1:Expression>{e}</d7p1:Expression>");
            return "";
        }

        // .............................................................................................

        public override string VisitExpr(pjc2Parser.ExprContext ctx)
        {
            var e = correctexpr(ctx.GetText());
            LC_WriteLine(ctx.Depth(), $@"<_expression>{e}</_expression>");
            return "";
        }

        // .............................................................................................

        public override string VisitValue(pjc2Parser.ValueContext ctx)
        {
            var e = correctexpr(ctx.GetText());
            //LC_WriteLine(ctx.Depth(), $@"<_value>{e}</_value>");
            stack.Push(e); // To VistValueParam
            return "";
        }

        // .............................................................................................

        public override string VisitIfelseexpr(pjc2Parser.IfelseexprContext ctx)
        {
            atext = "";
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);
            LC_WriteLine(ctx.Depth() + 1, "<!-- Condition -->");

            parent = id;

            // it has block behaviour
            // childs first
            string s = VisitChildren(ctx);

            LC_WriteLine(ctx.Depth() + 1, $"<Id>{Guid.NewGuid()}</Id>");
            parent = parents.Pop();
            LC_WriteLine(ctx.Depth() + 1, $@"<Parent z:Ref=""i{parent}""/>");

            // pop condition expression, it goes after children
            s = stack.Pop();
            LC_WriteLine(ctx.Depth(), s);

            LC_WriteLine(ctx.Depth() + 1, $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth() + 1, $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Text>{atext}</Text>");
            LC_WriteLine(ctx.Depth() + 1, "<_activityFullName>BR.Core.Base.If</_activityFullName>");
            LC_WriteLine(ctx.Depth(), "</AbstractStep>");
            atext = "";
            return s;
        }

        // .............................................................................................

        public override string VisitCondExpr(pjc2Parser.CondExprContext ctx)
        {
            // savecond expr to stack

            var sv = new System.IO.StringWriter();

            sv_WriteLine(ctx.Depth(), sv, "<PropertyValues>");            
            //sv_WriteLine(ctx.Depth() + 1, sv, $@"<PropertyValue z:Id=""i{++id}"">");
            StartPropertyValue(ctx.Depth() + 1, sv, ++id);
            sv_WriteLine(ctx.Depth() + 1, sv, "<PropertyName>Condition</PropertyName>");
            sv_WriteLine(ctx.Depth() + 1, sv, "<_dataType>Expression</_dataType>");

            var e = correctexpr(ctx.GetText());

            sv_WriteLine(ctx.Depth() + 1, sv, $@"<_expression>{e}</_expression>");

            sv_WriteLine(ctx.Depth() + 1, sv, @"<_value i:nil=""true"" />");
            EmitD7p1(ctx.Depth() + 1, sv);

            //sv_WriteLine(ctx.Depth() + 1, sv, "</PropertyValue>");
            EndPropertyValue(ctx.Depth() + 1, sv);
            sv_WriteLine(ctx.Depth(), sv, "</PropertyValues>");
            string s = sv.ToString();
            stack.Push(s);  // to "Condition"
            return s;
        }

        // .....................................................................................................

        public override string VisitIfblock(pjc2Parser.IfblockContext ctx)
        {

            /* Two blocks come together  - if and else*/
            LC_WriteLine(ctx.Depth(), "<Childs>");
            
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);
            
            LC_WriteLine(ctx.Depth() + 1, "<!-- IfTrue -->");

            parent = id;
            
            string s = VisitChildren(ctx);
            
            parent = parents.Pop();
            LC_WriteLine(ctx.Depth() + 1, $"<Id>{Guid.NewGuid()}</Id>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Parent z:Ref=""i{parent}""/>");
            LC_WriteLine(ctx.Depth() + 1, "<PropertyValues />");

            LC_WriteLine(ctx.Depth() + 1, $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth() + 1, $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Text></Text>");
            LC_WriteLine(ctx.Depth() + 1, "<_activityFullName>BR.Core.Base.IfTrue</_activityFullName>");
            
            LC_WriteLine(ctx.Depth(), "</AbstractStep>");

            return "if";
        }

        // .....................................................................................................

        public override string VisitElseblock(pjc2Parser.ElseblockContext ctx)
        {
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);
            
            LC_WriteLine(ctx.Depth() + 1, "<!-- IfFalse -->");

            parent = id;
            
            string s = VisitChildren(ctx);
            
            parent = parents.Pop();
            LC_WriteLine(ctx.Depth() + 1, $"<Id>{Guid.NewGuid()}</Id>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Parent z:Ref=""i{parent}""/>");
            LC_WriteLine(ctx.Depth() + 1, "<PropertyValues />");

            LC_WriteLine(ctx.Depth() + 1, $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth() + 1, $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Text></Text>");
            LC_WriteLine(ctx.Depth() + 1, "<_activityFullName>BR.Core.Base.IfFalse</_activityFullName>");
            
            EndAbstractStep(ctx.Depth());
            /* Two blocks come together */

            LC_WriteLine(ctx.Depth(), "</Childs>");

            return "else";
        }

        // .............................................................................................

        public override string VisitWhileexpr(pjc2Parser.WhileexprContext ctx)
        {
            atext = "";
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);
            
            LC_WriteLine(ctx.Depth() + 1, "<!-- Loop -->");

            parent = id;

            // it has block behaviour
            // children first
            string s = VisitChildren(ctx);

            LC_WriteLine(ctx.Depth() + 1, $"<Id>{Guid.NewGuid()}</Id>");
            parent = parents.Pop();
            LC_WriteLine(ctx.Depth() + 1, $@"<Parent z:Ref=""i{parent}""/>");

            s = stack.Pop(); // property values

            LC_WriteLine(ctx.Depth(), s);

            LC_WriteLine(ctx.Depth() + 1, $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth() + 1, $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Text>{atext}</Text>");
            LC_WriteLine(ctx.Depth() + 1, "<_activityFullName>BR.Core.Base.LoopWhile</_activityFullName>");
            
            LC_WriteLine(ctx.Depth(), "</AbstractStep>");
            atext = "";
            return s;
        }

        // .............................................................................................

        public override string VisitForeach(pjc2Parser.ForeachContext ctx)
        {
            atext = "";
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);
            
            LC_WriteLine(ctx.Depth() + 1, "<!-- ForEach -->");

            parent = id;

            string s = VisitChildren(ctx);

            EmitNewGuid(ctx.Depth() + 1);
            parent = parents.Pop();
            LC_WriteLine(ctx.Depth() + 1, $@"<Parent z:Ref=""i{parent}""/>");

            string forexpr = stack.Pop();
            string iter = stack.Pop();
            LC_WriteLine(ctx.Depth() + 1, "<PropertyValues>");
            LC_WriteLine(ctx.Depth() + 2, forexpr);
            LC_WriteLine(ctx.Depth() + 2, iter);
            LC_WriteLine(ctx.Depth() + 1, "</PropertyValues>");

            LC_WriteLine(ctx.Depth() + 1, $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth() + 1, $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Text>{atext}</Text>");
            LC_WriteLine(ctx.Depth() + 1, "<_activityFullName>BR.Core.Base.LoopForEach</_activityFullName>");
            
            LC_WriteLine(ctx.Depth(), "</AbstractStep>");
            atext = "";
            return s;
        }

        // .............................................................................................

        public override string VisitIterator(pjc2Parser.IteratorContext ctx)
        {
            var sv = new System.IO.StringWriter();

            StartPropertyValue(ctx.Depth(), sv, ++id);
            sv_WriteLine(ctx.Depth(), sv, "<PropertyName>Iterator</PropertyName>");
            sv_WriteLine(ctx.Depth(), sv, "<_dataType>Expression</_dataType>");

            sv_WriteLine(ctx.Depth(), sv, $@"<_expression>{ctx.GetText()}</_expression>");

            sv_WriteLine(ctx.Depth(), sv, @"<_value i:nil=""true"" />");

            EmitD7p1(ctx.Depth(), sv);
            EndPropertyValue(ctx.Depth(), sv);
            string s = sv.ToString();
            stack.Push(s);  // to "Collection"
            return s;
        }

        // .............................................................................................

        public override string VisitForExpr(pjc2Parser.ForExprContext ctx)
        {
            var sv = new System.IO.StringWriter();

            sv_WriteLine(ctx.Depth(), sv, $@"<PropertyValue z:Id=""i{++id}"">");
            
            sv_WriteLine(ctx.Depth(), sv, "<PropertyName>Collection</PropertyName>");
            sv_WriteLine(ctx.Depth(), sv, "<_dataType>Expression</_dataType>");

            var e = correctexpr(ctx.GetText());

            sv_WriteLine(ctx.Depth(), sv, $@"<_expression>{e}</_expression>");

            sv_WriteLine(ctx.Depth(), sv, @"<_value i:nil=""true"" />");
            EmitD7p1(ctx.Depth(), sv);
            sv_WriteLine(ctx.Depth(), sv, "</PropertyValue>");
            string s = sv.ToString();
            stack.Push(s);  // to "Collection"
            return s;
        }

        // .............................................................................................

        public override string VisitTrycatch(pjc2Parser.TrycatchContext ctx)
        {
            atext = "";
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);
            
            LC_WriteLine(ctx.Depth(), "<!-- trycatch -->");

            parent = id;

            // it has block behaviour

            string s = VisitChildren(ctx);

            LC_WriteLine(ctx.Depth(), $"<Id>{Guid.NewGuid()}</Id>");
            parent = parents.Pop();
            LC_WriteLine(ctx.Depth(), $@"<Parent z:Ref=""i{parent}""/>");

            LC_WriteLine(ctx.Depth(), "<PropertyValues />");

            LC_WriteLine(ctx.Depth(), $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth(), $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth(), $@"<Text>{atext}</Text>");
            LC_WriteLine(ctx.Depth(), "<_activityFullName>BR.Core.Base.TryCatch</_activityFullName>");
            
            LC_WriteLine(ctx.Depth(), "</AbstractStep>");
            atext = "";
            return s;
        }

        // ..............................................................................................

        public override string VisitTryblock(pjc2Parser.TryblockContext ctx)
        {
            /* Two blocks come together */
            LC_WriteLine(ctx.Depth(), "<Childs>");
            
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);
            
            LC_WriteLine(ctx.Depth(), "<!-- Try -->");

            parent = id;
            
            string s = VisitChildren(ctx);
            
            parent = parents.Pop();
            EmitNewGuid(ctx.Depth());
            LC_WriteLine(ctx.Depth(), $@"<Parent z:Ref=""i{parent}""/>");
            LC_WriteLine(ctx.Depth(), "<PropertyValues />");

            LC_WriteLine(ctx.Depth(), $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth(), $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth(), $@"<Text></Text>");
            LC_WriteLine(ctx.Depth(), "<_activityFullName>BR.Core.Base.Try</_activityFullName>");
            
            LC_WriteLine(ctx.Depth(), "</AbstractStep>");

            return "try";
        }

        // ...............................................................................................

        public override string VisitCatchblock(pjc2Parser.CatchblockContext ctx)
        {
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);
            
            LC_WriteLine(ctx.Depth(), "<!-- Catch -->");

            parent = id;
            
            string s = VisitChildren(ctx); // catch block
            
            parent = parents.Pop();
            LC_WriteLine(ctx.Depth(), $"<Id>{Guid.NewGuid()}</Id>");
            LC_WriteLine(ctx.Depth(), $@"<Parent z:Ref=""i{parent}""/>");

            string props = stack.Pop();
            LC_WriteLine(ctx.Depth(), "<PropertyValues>");
            LC_WriteLine(ctx.Depth(), props);
            LC_WriteLine(ctx.Depth(), "</PropertyValues>");

            LC_WriteLine(ctx.Depth(), $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth(), $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth(), $@"<Text></Text>");
            LC_WriteLine(ctx.Depth(), "<_activityFullName>BR.Core.Base.Catch</_activityFullName>");
            
            LC_WriteLine(ctx.Depth(), "</AbstractStep>");

            /* Two blocks come together */
            
            LC_WriteLine(ctx.Depth(), "</Childs>");

            return "catch";
        }

        // .............................................................................................

        public override string VisitCatchParams(pjc2Parser.CatchParamsContext ctx)
        {
            var sv = new System.IO.StringWriter();
            string pars = ctx.GetText();
            string[] ap = Regex.Split(pars, @"[,\s]+");

            if (ap.Length != 3)
            {
                throw (new Exception("catch parameters count must be 3"));
            }
            
            StartPropertyValue(ctx.Depth(), sv, ++id);
            EmitPropertyName(ctx.Depth(), sv, "Message");
            sv_WriteLine(ctx.Depth(), sv, "<_dataType>Expression</_dataType>");
            sv_WriteLine(ctx.Depth(), sv, $"<_expression>{ap[0]}</_expression>");
            sv_WriteLine(ctx.Depth(), sv, @"<_value i:nil=""true"" />");
            EmitD9p1(ctx.Depth(), sv);

            EndPropertyValue(ctx.Depth(), sv);

            // .....
            StartPropertyValue(ctx.Depth(), sv, ++id);

            EmitPropertyName(ctx.Depth(), sv, "StackTrace");
            sv_WriteLine(ctx.Depth(), sv, "<_dataType>Expression</_dataType>");
            sv_WriteLine(ctx.Depth(), sv, $"<_expression>{ap[1]}</_expression>");

            sv_WriteLine(ctx.Depth(), sv, @"<_value i:nil=""true"" />");
            EmitD9p1(ctx.Depth(), sv);
            EndPropertyValue(ctx.Depth(), sv);

            // .....
            StartPropertyValue(ctx.Depth(), sv, ++id);

            EmitPropertyName(ctx.Depth(), sv, "Source");
            sv_WriteLine(ctx.Depth(), sv, "<_dataType>Expression</_dataType>");
            sv_WriteLine(ctx.Depth(), sv, $"<_expression>{ap[2]}</_expression>");

            sv_WriteLine(ctx.Depth(), sv, @"<_value i:nil=""true"" />");
            EmitD9p1(ctx.Depth(), sv);

            EndPropertyValue(ctx.Depth(), sv);

            string s = sv.ToString();
            stack.Push(s);  // to "Catch"
            return s;
        }

        // .............................................................................................

        public override string VisitTryfix(pjc2Parser.TryfixContext ctx)
        {
            atext = "";
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);
            
            LC_WriteLine(ctx.Depth(), "<!-- TryFix -->");

            parent = id;

            // it has block behaviour
            // childsw first
            string s = VisitChildren(ctx);

            EmitNewGuid(ctx.Depth());
            parent = parents.Pop();
            LC_WriteLine(ctx.Depth(), $@"<Parent z:Ref=""i{parent}""/>");

            // pop param
            s = stack.Pop();
            LC_WriteLine(ctx.Depth(), s);

            LC_WriteLine(ctx.Depth(), $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth(), $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth(), $@"<Text>{atext}</Text>");
            LC_WriteLine(ctx.Depth(), "<_activityFullName>BR.Core.Base.TryFix</_activityFullName>");
            
            EndAbstractStep(ctx.Depth());
            atext = "";
            return s;
        }

        // .............................................................................................

        public override string VisitTryparam(pjc2Parser.TryparamContext ctx)
        {
            // save param to stack

            var sv = new System.IO.StringWriter();

            sv_WriteLine(ctx.Depth(), sv, "<PropertyValues>");
            
            sv_WriteLine(ctx.Depth(), sv, $@"<PropertyValue z:Id=""i{++id}"">");
            
            sv_WriteLine(ctx.Depth(), sv, "<PropertyName>StartWithErrorStep</PropertyName>");
            sv_WriteLine(ctx.Depth(), sv, "<_dataType>Value</_dataType>");

            EmitD7p1(ctx.Depth(), sv);

            sv_WriteLine(ctx.Depth(), sv, @"<_expression i:nil=""true"" />");
            EmitD7p1(ctx.Depth(), sv);
            sv_WriteLine(ctx.Depth(), sv, "</PropertyValue>");
            
            sv_WriteLine(ctx.Depth(), sv, "</PropertyValues>");
            string s = sv.ToString();
            stack.Push(s);  // to "tryfix"
            return s;
        }

        // ...............................................................................................

        public override string VisitFixblock(pjc2Parser.FixblockContext ctx)
        {
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);
            
            LC_WriteLine(ctx.Depth(), "<!-- Fix -->");

            parent = id;
            
            string s = VisitChildren(ctx); // catch block
            
            parent = parents.Pop();
            LC_WriteLine(ctx.Depth(), $"<Id>{Guid.NewGuid()}</Id>");
            LC_WriteLine(ctx.Depth(), $@"<Parent z:Ref=""i{parent}""/>");

            string props = stack.Pop();
            LC_WriteLine(ctx.Depth(), "<PropertyValues>");
            LC_WriteLine(ctx.Depth(), props);
            LC_WriteLine(ctx.Depth(), "</PropertyValues>");

            LC_WriteLine(ctx.Depth(), $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth(), $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth(), $@"<Text></Text>");
            LC_WriteLine(ctx.Depth(), "<_activityFullName>BR.Core.Base.Fix</_activityFullName>");
            
            LC_WriteLine(ctx.Depth(), "</AbstractStep>");

            /* Two blocks come together */
            
            LC_WriteLine(ctx.Depth(), "</Childs>");

            return "fix";
        }

        // ...............................................................................................

        public override string VisitReturn(pjc2Parser.ReturnContext ctx)
        {
            StartAbstractStep(ctx.Depth(), ++id);

            LC_WriteLine(ctx.Depth(), "<!-- return -->");
            LC_WriteLine(ctx.Depth(), "<Childs />");            

            LC_WriteLine(ctx.Depth(), $"<Id>{Guid.NewGuid()}</Id>");
            LC_WriteLine(ctx.Depth(), $@"<Parent z:Ref=""i{parent}""/>");
            LC_WriteLine(ctx.Depth(), "<PropertyValues />");
            LC_WriteLine(ctx.Depth(), $@"<SelectedOptionId i:nil=""true"" />");
            LC_WriteLine(ctx.Depth(), "<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth(), "<Text></Text>");
            LC_WriteLine(ctx.Depth(), "<_activityFullName>BR.Core.Base.Return</_activityFullName>");
            
            LC_WriteLine(ctx.Depth(), "</AbstractStep>");

            return "";
        }

        // ...............................................................................................

        public override string VisitComment(pjc2Parser.CommentContext ctx)
        {
            LC_WriteLine(ctx.Depth(), $@"<AbstractStep z:Id=""i{++id}"" i:type=""CommentStep"">");
            
            LC_WriteLine(ctx.Depth() + 1, "<!-- comment -->");
            LC_WriteLine(ctx.Depth() + 1, "<Childs />");

            LC_WriteLine(ctx.Depth() + 1, $"<Id>{Guid.NewGuid()}</Id>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Parent z:Ref=""i{parent}""/>");
            LC_WriteLine(ctx.Depth() + 1, "<PropertyValues />");
            LC_WriteLine(ctx.Depth() + 1, $@"<SelectedOptionId i:nil=""true"" />");
            string text = Regex.Replace(ctx.GetText(), @"(^!--)|(\r|\n)+$", "");
            LC_WriteLine(ctx.Depth() + 1, $"<Text>{text}</Text>");            
            LC_WriteLine(ctx.Depth(), "</AbstractStep>");

            return "";
        }

        // ...............................................................................................

        public override string VisitContainer(pjc2Parser.ContainerContext ctx)
        {
            StartAbstractStep(ctx.Depth(), ++id);
            parents.Push(parent);

            LC_WriteLine(ctx.Depth() + 1, "<!-- container -->");

            parent = id;

            string s = VisitChildren(ctx);

            parent = parents.Pop();
            LC_WriteLine(ctx.Depth() + 1, $"<Id>{Guid.NewGuid()}</Id>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Parent z:Ref=""i{parent}""/>");
            LC_WriteLine(ctx.Depth() + 1, "<PropertyValues />");

            LC_WriteLine(ctx.Depth() + 1, $@"<SelectedOptionId i:nil=""true""/>");
            LC_WriteLine(ctx.Depth() + 1, $@"<EnableStatus>true</EnableStatus>");
            LC_WriteLine(ctx.Depth() + 1, $@"<Text></Text>");
            LC_WriteLine(ctx.Depth() + 1, "<_activityFullName>BR.Core.Base.Container</_activityFullName>");

            LC_WriteLine(ctx.Depth(), "</AbstractStep>");

            return "";
        }

    }

    // ..............................................................................................

    class Compiler
    {

        public static void Main(string[] args)
        {
            if(args.Length < 1)
            {
                Console.WriteLine("PJCompiler <scriptfile>");
                return;
            }
            try
            {
                string input2;
                string infile = args[0];
                string outfile = "~$" + infile + ".pix";
                input2 = System.IO.File.ReadAllText(infile);
                LC.Open(outfile);
                try
                {
                    var stream = new AntlrInputStream(input2);
                    ITokenSource lexer = new pjc2Lexer(stream);
                    ITokenStream tokens = new CommonTokenStream(lexer);

                    var parser = new pjc2Parser(tokens);
                    //parser.BuildParseTree = true;
                    var tree = parser.compileUnit();

                    var v = new Visitor();

                    try
                    {
                        v.Visit(tree);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception: " + ex.Message);
                    }
                } finally
                {
                    LC.Close();
                }
                if (System.IO.File.Exists(LC.ScriptName))
                {
                    System.IO.File.Delete(LC.ScriptName);
                }
                System.IO.File.Move(LC.FileName, LC.ScriptName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
        }
    }
}
