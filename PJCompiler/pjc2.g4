grammar pjc2;

/*
 * Parser Rules
 */


 script: scriptName '(' scriptArgList? ')' logic ;

 logic : block ;

scriptName: Ident;

scriptArgList: scriptArg (',' scriptArg)* ;

scriptArg: scriptArgName ':' scripArgType (',' defVal)? ;

scriptArgName: Ident;
scripArgType: ArgType;
defVal: expr;

 seq: callact + ;

 callact : activity | whileexpr | ifelseexpr | foreach | trycatch | tryfix | return | comment | container | assignment;

 assignment : assignIdent '=' assignExpr ';';

 assignIdent : Ident;
 assignExpr : Expr;

 comment: Comment ;

 container:  block | emptyblock;

 // emptycontainer: '{' '}';

// activity: (actName (':' 'option' '=' option)? (':' 'text' '=' atext)? '(' plist? ')' (';' | block)) ;

 // without block tempor.
 activity: actName (':' 'option' '=' option)? (':' 'text' '=' atext)? '(' plist? ')' ';'  ;

 option: Digit;

 atext: Text ;

 actName: ActName;

 whileexpr: 'while' (':' 'text' '=' atext)? '(' condExpr ')' whileblock ;
 whileblock:  (block | emptyblock);

 ifelseexpr: 'if' (':' 'text' '=' atext)? '(' condExpr ')' ifblock (elseblock) ;
 
 ifblock:  block | emptyblock;
 elseblock: 'else' (block | emptyblock);

 condExpr: Expr;

 emptyblock: '{' '}' ;

 foreach: 'foreach' (':' 'text' '=' atext)? '(' iterator 'in' forExpr ')' (forblock | emptyblock);
  
 iterator: Ident;
 forExpr: Expr;
 forblock: block | emptyblock;

 trycatch: 'try' (':' 'text' '=' atext)? tryblock 'catch' '(' catchParams ')' catchblock;
 tryblock:  (block | emptyblock);
 catchblock: block | emptyblock;

 catchParams: Ident ',' Ident ',' Ident;

 tryfix: 'try' (':' 'text' '=' atext)? '(' tryparam ')' tryblock 'fix' '(' catchParams ')' fixblock;

tryparam: 'true' | 'false';
fixblock: block | emptyblock;

return: 'return' ';';

 plist: param (',' param)* ;

 param: (exprParam | valueParam) | arglist ;

 exprParam: paramname ':' expr ;
 valueParam: paramname ':' value ':' paramType;

 paramname: Ident ;
 paramType: argType;

 arglist: '{' arg (';' arg)* '}' ;

 arg: argName ':' argVal ',' argType ;

 argName: Ident ;
 argType: ArgType ;
 argVal: Expr;

 block: '{' seq '}' ;

 expr : Expr ;
 value : Value ;

compileUnit
	:	script EOF
	;

/*
 * Lexer Rules
 */
// � - code 1057
 Ident: [_A-Za-z\u0421] [_0-9A-Za-z\u0421]* ;

 ActName: ('@'|'$') Ident ( '.' Ident)*;

 ArgType : '%' Ident ( '.' Ident)* ;

 Digit : [0-9] ;

 Expr: ('[[' .+? ']]') | ('`' .+? '`') ;
 Value: '[`' .+? '`]' ;

 Text: '"' .+? '"' ;

 Comment: '!--'.*? ('\n'|'\r') ;

 COMMENT : '//' .*? ('\n'|'\r') -> channel(HIDDEN);

WS
	:	[ \r\n\t\f] -> channel(HIDDEN)
	;
